//[SECTION] Modules and Dependencies
	const mongoose = require('mongoose');

//[SECTION] Schema/Blueprint
	
	const orderSchema = new mongoose.Schema({
				userId: {
					type: String,
					
				},
				products: [
				{
				productId:{
					type: String,
				
				},
				name:{
					type: String,
					
				},
				price:{
					type: Number,
					
				},
				quantity:{
					type: Number,
					
				},
				}
				],
				totalAmount: {
					type: Number,
					
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				}
	});

//[SECTION]Model
 	module.exports = mongoose.model('Order' , orderSchema);