//[SECTION] Dependencies and Modules
   const Order = require('../models/Order')
   const Product = require('../models/Product');
   const User = require('../models/User');  
   const bcrypt = require('bcrypt');
   const dotenv = require('dotenv');
   const auth = require('../auth.js');

//[SECTION]Environment variable Setup
	dotenv.config();
	const asin = Number(process.env.SALT);
	

//[SECTION] Functionalities [CREATE]
	
	 module.exports.addOrder = (req, res) => {
	
	console.log(req.body.productId)	
	
		
		let productOrder = Product.findById(req.body.productId).then(product => {
		let newOrder = new Order ({
			userId: req.user.id,
			products: [{
			productId: product.id,
			name: product.productName,
			price: product.price,
			quantity: req.body.quantity,
			}],
			totalAmount: product.price * req.body.quantity
			});
		
		return newOrder.save().then((user, err) =>{
			
			if (user) {
				return res.send ({message: "complete"})
			} else {
				return 'Error';

			};
		})
	});

};

