//[SECTION] Dependencies and Modules
   const User = require('../models/User')  
   const bcrypt = require('bcrypt');
   const dotenv = require('dotenv');
   const auth = require('../auth.js');

//[SECTION]Environment variable Setup
	dotenv.config();
	const asin = Number(process.env.SALT);
	

//[SECTION] Functionalities [CREATE]
	//1. Register New Account
	 module.exports.register = (userData) => {
		console.log(userData)
		let email = userData.email;
		let passW  = userData.password;
	
		
		let newUser = new User({
			email: email,
			password: bcrypt.hashSync(passW, asin),
			
		});
		
		return newUser.save().then((user, err) =>{
			
			if (user) {
				return user;
			} else {
				return 'Error';
			};
		});

	};


	module.exports.loginUser = (data) => {
		//findOne method returns the first record in the collection that matches the search criteria
		return User.findOne({ email: data.email }).then(result => {
			//if user does not exist
			if (result == null) {
				return false;
			} else {
				//user exists
				//'compareSync'method from the bcrypt to used in comparing the non encrypted password from the login and the database password. it return 'true' or 'false' depending on the result
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
				//if the password match, return token
				if (isPasswordCorrect) {
					return {accessToken: auth.createAccessToken(result.toObject())}
				} else {
					//password do not match
					return false;
				}
			}
		});
	}