const exp = require('express');   
const controller = require('../controller/users'); 

const route = exp.Router();


	route.post('/register', (req, res) =>{
		
		let userData = req.body;
		controller.register(userData).then(outcome =>{
			res.send(outcome);
		});	
	});

	
	
	route.post('/login',(req, res) =>{
		controller.loginUser(req.body).then(result => res.send(result));
	})


	module.exports = route;

