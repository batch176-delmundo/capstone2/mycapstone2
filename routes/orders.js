const express = require ('express');
const route = express.Router();
const OrderController = require('../controller/orders');
const auth = require('../auth');


const {verify, verifyAdmin } = auth;



route.post('/addOrder', auth.verify, OrderController.addOrder)


module.exports = route;