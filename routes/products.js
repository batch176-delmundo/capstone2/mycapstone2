const express = require ('express');
const route = express.Router();
const ProductController = require('../controller/products');
const auth = require('../auth');



const {verify, verifyAdmin } = auth;

route.post('/addProduct', verify, verifyAdmin, (req, res ) =>{
	ProductController.addProduct(req.body).then(result => res.send(result))
})


route.get('/active', (req, res )=> {
	ProductController.getAllActiveProducts().then(result =>res.send(result));
})


route.get('/:productId', (req, res ) =>{
	console.log(req.params.courseId)
	
	ProductController.getProduct(req.params.productId).then(result => res.send(result));
})


route.put('/:productId', verify, verifyAdmin, (req,res)=>{
	ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
})


route.put('/:productId/archive', verify, verifyAdmin, (req, res ) =>{
	ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
})


module.exports = route;